import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConsumptionComponent } from './consumption.component';
import { UsageComponent } from './usage/usage.component';

const routes: Routes = [{
  path: '',
  component: ConsumptionComponent,
  children: [
  { path: 'usage', component: UsageComponent }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsumptionRoutingModule { }

export const routedComponents = [
  ConsumptionComponent,
  UsageComponent,
];
