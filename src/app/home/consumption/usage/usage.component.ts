import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { SmartTableService } from '../../../@core/data/smart-table.service';
import { CommonService, Usage } from '../../../_dbShare';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'ngx-usage',
  templateUrl: './usage.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
  providers : [CommonService]
})
export class UsageComponent implements OnInit {

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      applianceName: {
        title: 'Appliance',
        type: 'string',
      },
      on_time: {
        title: 'On Time',
        type: 'DateTime',
      },
      off_time: {
        title: 'Off Time',
        type: 'DateTime',
      },
      unit: {
        title: 'Usage Unit',
        type: 'number',
      },
      amount: {
        title: 'Consumption Amount',
        type: 'number',
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  usages: Usage[];
  length: number;

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(private service: SmartTableService,
      private commonService: CommonService) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit() {
    this.getUsage();
  }

  getUsage() {
    let params = { 
          // 'paginate': this.pageSize,
          // 'page':this.pageNumber,
          // 'search': this.searchInput 
        };
      this.commonService.getAll('usage', params)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(data => {
          this.length = data.usages.total;
          this.usages = data.usages.data;
          this.source.load(this.usages);
        },
        error => console.log('Error ::' + error)
      );
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
