import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { ConsumptionRoutingModule, routedComponents } from './consumption-routing.module';
import { CommonService } from '../../_dbShare';

@NgModule({
  imports: [
    ThemeModule,
    ConsumptionRoutingModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
    CommonService,
  ],
})
export class ConsumptionModule { }
