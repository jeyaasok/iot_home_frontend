import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: '/home/dashboard',
    home: true,
  },
  {
    title: 'FEATURES',
    group: true,
  },
  {
    title: 'Masters',
    icon: 'nb-tables',
    children: [
      {
        title: 'Appliances',
        link: '/home/master/appliance',
      },
      {
        title: 'Floor',
        link: '/home/master/floor',
      },
      {
        title: 'Rooms',
        link: '/home/master/room',
      },
      {
        title: 'status',
        link: '/home/master/status',
      },
    ],
  },
  {
    title: 'Consumption',
    icon: 'nb-tables',
    children: [
      {
        title: 'Usage',
        link: '/home/consumption/usage',
      },
    ],
  },
  {
    title: 'Account',
    icon: 'nb-tables',
    children: [
      {
        title: 'Invoice',
        link: '/home/account/invoice',
      },
    ],
  },
  
];
