import { Component, Input, OnInit } from '@angular/core';
import { CommonService } from '../../../_dbShare';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'ngx-status-card',
  styleUrls: ['./status-card.component.scss'],
  providers: [CommonService],
  template: `
    <nb-card (click)="changeState(item.id)" [ngClass]="{'off': !on}">
      <div class="icon-container">
        <div class="icon {{ type }}">
          <ng-content></ng-content>
        </div>
      </div>

      <div class="details">
        <div class="title">{{ item.appliance?.name | titlecase }}</div>
        <div class="status">{{ on ? 'ON' : 'OFF' }}</div>
      </div>
    </nb-card>
  `,
})
export class StatusCardComponent implements OnInit {

  @Input() item: any;
  @Input() type: string;
  @Input() on = true;
  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(private commonService: CommonService) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit() {
    this.on = (this.item.state == 'on') ? true : false;
  }

  changeState(id) {
    this.item.state = (this.item.state == 'on') ? 'off' : 'on';
    let params = { 
      'state': this.item.state,
      'equipment_watts':this.item.appliance.watts
    };
    this.commonService.updateItem('appliance-place', id, params)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(data => {
          this.on = !this.on;
        },
        error => console.log('Error ::' + error)
      );
    
  }

}
