import { Component } from '@angular/core';

import { MENU_ITEMS } from './Home-menu';

@Component({
  selector: 'ngx-home',
  template: `
    <ngx-sample-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-sample-layout>
  `,
})
export class HomeComponent {

  menu = MENU_ITEMS;
}
