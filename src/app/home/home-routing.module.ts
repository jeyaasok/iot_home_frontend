import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { HomeComponent } from './Home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';

const routes: Routes = [{
  path: '',
  component: HomeComponent,
  children: [{
    path: 'dashboard',
    component: DashboardComponent,
  }, 
  {
    path: 'master',
    loadChildren: './master/master.module#MasterModule',
  }, 
  {
    path: 'consumption',
    loadChildren: './consumption/consumption.module#ConsumptionModule',
  }, 
  {
    path: 'account',
    loadChildren: './account/account.module#AccountModule',
  }, 
  // {
  //   path: 'miscellaneous',
  //   loadChildren: './miscellaneous/miscellaneous.module#MiscellaneousModule',
  // }, 
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  }, 
  {
    path: '**',
    component: NotFoundComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {
}
