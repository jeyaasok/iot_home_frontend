import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { SmartTableService } from '../../../@core/data/smart-table.service';
import { CommonService, Invoice } from '../../../_dbShare';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'ngx-invoice',
  templateUrl: './invoice.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
  providers : [CommonService]
})
export class InvoiceComponent implements OnInit {

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      code: {
        title: 'Code',
        type: 'string',
      },
      from_date: {
        title: 'From',
        type: 'date',
      },
      to_date: {
        title: 'To',
        type: 'date',
      },
      total_amount: {
        title: 'Total Amount',
        type: 'number',
      },
      is_paid: {
        title: 'Is Paided',
        type: 'number',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  invoices: Invoice[];
  length: number;

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(private service: SmartTableService,
      private commonService: CommonService) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit() {
    this.getInvoice();
  }

  getInvoice() {
    let params = { 
          // 'paginate': this.pageSize,
          // 'page':this.pageNumber,
          // 'search': this.searchInput 
        };
      this.commonService.getAll('invoice', params)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(data => {
          this.length = data.invoices.total;
          this.invoices = data.invoices.data;
          this.source.load(this.invoices);
        },
        error => console.log('Error ::' + error)
      );
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
