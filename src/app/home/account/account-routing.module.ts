import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccountComponent } from './account.component';
import { InvoiceComponent } from './invoice/invoice.component';

const routes: Routes = [{
  path: '',
  component: AccountComponent,
  children: [
  { path: 'invoice', component: InvoiceComponent }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountRoutingModule { }

export const routedComponents = [
  AccountComponent,
  InvoiceComponent,
];
