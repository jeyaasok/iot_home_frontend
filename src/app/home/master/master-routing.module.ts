import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MasterComponent } from './master.component';
import { ApplianceComponent } from './appliance/appliance.component';
import { FloorComponent } from './floor/floor.component';
import { RoomComponent } from './room/room.component';
import { StatusComponent } from './status/status.component';

const routes: Routes = [{
  path: '',
  component: MasterComponent,
  children: [
  { path: 'appliance', component: ApplianceComponent },
  { path: 'floor', component: FloorComponent },
  { path: 'room', component: RoomComponent },
  { path: 'status', component: StatusComponent }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MasterRoutingModule { }

export const routedComponents = [
  MasterComponent,
  ApplianceComponent,
  FloorComponent,
  RoomComponent,
  StatusComponent
];
