import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { MasterRoutingModule, routedComponents } from './master-routing.module';
import { CommonService } from '../../_dbShare';

@NgModule({
  imports: [
    ThemeModule,
    MasterRoutingModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
    CommonService,
  ],
})
export class MasterModule { }
