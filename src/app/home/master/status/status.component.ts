import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { SmartTableService } from '../../../@core/data/smart-table.service';
import { CommonService, Status } from '../../../_dbShare';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'ngx-status',
  templateUrl: './status.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
  providers : [CommonService]
})
export class StatusComponent implements OnInit {

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      name: {
        title: 'Name',
        type: 'string',
      },
      description: {
        title: 'Description',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  statuses: Status[];
  length: number;

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(private service: SmartTableService,
      private commonService: CommonService) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit() {
    this.getAppliance();
  }

  getAppliance() {
    let params = { 
          // 'paginate': this.pageSize,
          // 'page':this.pageNumber,
          // 'search': this.searchInput 
        };
      this.commonService.getAll('status', params)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(data => {
          this.length = data.statuses.total;
          this.statuses = data.statuses.data;
          this.source.load(this.statuses);
        },
        error => console.log('Error ::' + error)
      );
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
