import { NgModule } from '@angular/core';

import { HomeComponent } from './Home.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { HomeRoutingModule } from './home-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { CommonService } from '../_dbShare';

const PAGES_COMPONENTS = [
  HomeComponent,
];

@NgModule({
  imports: [
    HomeRoutingModule,
    ThemeModule,
    DashboardModule,
    MiscellaneousModule
  ],
  declarations: [
    ...PAGES_COMPONENTS,
  ],
  providers: [
    CommonService,
  ]
})
export class HomeModule {
}
