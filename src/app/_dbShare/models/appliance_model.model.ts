export class ApplianceModel {
  id: number;
  name: string;
  slug: string;
  brand: string;
  current: number;
  voltage: number;
  watts: number;
  description: string;
  created_by: number;
  updated_by: number;
  created_at: Date;
  updated_at: Date;
  deleted_at: Date;

  applianceOnPlaces?: any[];

  constructor(appliance)
    {
      {
        this.id = appliance.id || '';
        this.name = appliance.name || '';
        this.slug = appliance.slug || '';
        this.brand = appliance.brand || '';
        this.current = appliance.current || '';
        this.voltage = appliance.voltage || '';
        this.watts = appliance.watts || '';
        this.description = appliance.description || '';
      }
    }
}