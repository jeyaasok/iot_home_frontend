export interface Invoice {
  id: number;
  code: string;
  from_date: Date;
  to_date: Date;
  total_amount: number;
  is_paid: boolean;
  created_by: number;
  updated_by: number;
  created_at: Date;
  updated_at: Date;
  deleted_at: Date;

  usages?: any[];
}
