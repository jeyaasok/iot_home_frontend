export interface Usage {
  id: number;
  appliance_on_place_id: number;
  on_time: Date;
  off_time: Date;
  used_time: number;
  equipment_watts: number;
  unit: number;
  amount: number;
  is_paid: boolean;
  invoice_id: boolean;
  created_by: number;
  updated_by: number;
  created_at: Date;
  updated_at: Date;
  deleted_at: Date;

  invoice?: {
    id: number;
    code: string;
    from_date: Date;
    to_date: Date;
    total_amount: number;
    is_paid: boolean;
    created_by: number;
    updated_by: number;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
  }

  applianceOnPlace?: {
    id: number;
    appliance_id: number;
    floor_id: number;
    room_id: number;
    status_id: number;
    state: string;
    created_by: number;
    updated_by: number;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
  }
}
