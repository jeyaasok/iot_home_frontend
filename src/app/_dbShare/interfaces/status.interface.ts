export interface Status {
  id: number;
  name: string;
  slug: string;
  description: string;
  created_by: number;
  updated_by: number;
  created_at: Date;
  updated_at: Date;
  deleted_at: Date;

  applianceOnPlaces?: any[];
}
