export interface ApplianceOnPlace {
  id: number;
  appliance_id: number;
  floor_id: number;
  room_id: number;
  status_id: number;
  state: string;
  created_by: number;
  updated_by: number;
  created_at: Date;
  updated_at: Date;
  deleted_at: Date;

  appliance?: {
    id: number;
    name: string;
    slug: string;
    brand: string;
    current: number;
    voltage: number;
    watts: number;
    description: string;
    created_by: number;
    updated_by: number;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
  }

  floor?: {
    id: number;
    name: string;
    slug: string;
    description: string;
    created_by: number;
    updated_by: number;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
  }

  room?: {
    id: number;
    name: string;
    slug: string;
    description: string;
    created_by: number;
    updated_by: number;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
  }

  status?: {
    id: number;
    name: string;
    slug: string;
    description: string;
    created_by: number;
    updated_by: number;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
  }

  usages?: any[];
}
