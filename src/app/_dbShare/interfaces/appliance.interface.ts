export interface Appliance {
  id: number;
  name: string;
  slug: string;
  brand: string;
  current: number;
  voltage: number;
  watts: number;
  description: string;
  created_by: number;
  updated_by: number;
  created_at: Date;
  updated_at: Date;
  deleted_at: Date;

  applianceOnPlaces?: any[];
}
