import { Injectable } from '@angular/core';

@Injectable()
export class GlobalService {

  static company_name: string = 'Tech JOB India.';
  static company_author: string = 'Jeyakumar.A';
  static company_phone: string = '+91-9976648919';
  static company_mobile: string = '+91-9976648919';
  static company_website: string = 'www.techjobind.com';
  static company_email: string = 'support@techjobind.com';
  static company_address: string = '#57, Thulsairam Street, Meenakshi Nagar, Villapuram, Madurai - 625012.';


  public logo_image: string = '/api/image/2';
}
