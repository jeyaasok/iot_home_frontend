import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { GlobalService } from './global.service';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

@Injectable()
export class CommonService {

  items: any;
  item: any;
  onChanged: BehaviorSubject<any>;
  onSelected: BehaviorSubject<any>;

  constructor(private http: Http) {
    this.onChanged = new BehaviorSubject([]);
  }

  tji_domain: string = '/api/';
  header = new Headers({
    'Content-Type': 'application/json'
  });

  // Get All Items
  getAll(url: string, input:any=null): Observable<any> {
    return this.http.get(this.tji_domain + url, { headers: this.header, params: input})
    .map( (response: Response) => {
      this.items = response.json();
      this.onChanged.next(this.items);
      return this.items;
    });
  }

  // Get Item by Selected ID
  getItem(url: string, id: number) {
    return this.http.get(this.tji_domain + url + '/' + id, {headers: this.header})
    .map( (response: Response) => {
      this.item = response.json();
      this.onChanged.next(this.item);
      return this.item;
    });
  }

  // Store New Item Data
  storeItem(url: string, data: any) {
    const body = JSON.stringify(data);
    return this.http.post(this.tji_domain + url, body, {headers: this.header})
    .map( (response: Response) => {
      return response.json();
    });
  }

  // Update Item by Selected ID
  updateItem(url: string, id: number, data: any) {
    const body = JSON.stringify(data);
    return this.http.put(this.tji_domain + url + '/' + id, body, {headers: this.header})
    .map( (response: Response) => {
        return response.json();
      });
  }

  // Delete Item by Selected ID
  deleteItem(url: string, id: number): Observable<any> {
    return this.http.delete(this.tji_domain + url + '/' + id, {headers: this.header})
      .map( (response: Response) => {
        return response.json();
      });
  }

}




/**
 **************************************************************
 *                     Params HelpLine
 ***************************************************************
 *
 ********************* deleted_at  *****************************
 * Use {'deleted_at': 'only'} => can get only Deleted Records
 * Use {'deleted_at': 'all'} => can get all Record with Deleted datas
 * Use without params can get without Deleted Records.
 *
 *
 ********************** is_active:  *****************************
 * Use {'is_active': 0} => can get only InActive Records
 * Use {'is_active': 1} => can get all Active Records
 * Use without params can get all Active and InActive Records.
 */